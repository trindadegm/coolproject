use std::io::{self, BufRead, BufReader};

fn main() {
    println!("Qual o seu nome?");
    let mut reader = BufReader::new(io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();
    println!("Bom dia {}!", line.trim());
}
